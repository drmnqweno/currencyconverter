﻿using CurrencyConverter.Core.Domain.Entities;
using System.Threading.Tasks;

namespace CurrencyConverter.Core.Domain.Adaptors
{
    public interface ITrainlineCurrencyAdaptorTwo
    {
        Task<CurrencyConversion> GetExchangeRatesAsync(string currency);
    }
}
