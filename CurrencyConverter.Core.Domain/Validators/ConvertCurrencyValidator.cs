﻿using CurrencyConverter.Core.Domain.Enums;
using CurrencyConverter.Core.Domain.QueryModels.RequestModels;
using FluentValidation;
using System;

namespace CurrencyConverter.Core.Domain.Validators
{
    public class ConvertCurrencyValidator : AbstractValidator<ConvertCurrencyRequest>
    {
        public ConvertCurrencyValidator()
        {
            RuleFor(m => m.BaseCurrency)
                .NotEmpty()
                .WithMessage("Please provide the base currency");

            RuleFor(m => m.Price)
                .GreaterThan(0)
                .WithMessage("Price cannot be 0");

            RuleFor(m => m.TargetCurrency)
                .Must(m => Enum.TryParse<SupportedCurrencies>(m.ToUpper(), out var supportedCurrency))
                .WithMessage("Target currency is not supported");

        }
    }
}
