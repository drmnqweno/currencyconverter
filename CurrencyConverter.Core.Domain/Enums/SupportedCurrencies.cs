﻿namespace CurrencyConverter.Core.Domain.Enums
{
    public enum SupportedCurrencies
    {
        Invalid,
        EUR,
        GBP,
        USD
    }
}
