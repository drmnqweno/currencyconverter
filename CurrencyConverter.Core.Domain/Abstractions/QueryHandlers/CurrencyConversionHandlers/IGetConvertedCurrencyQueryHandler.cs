﻿using CurrencyConverter.Core.Domain.QueryModels;
using CurrencyConverter.Core.Domain.QueryModels.RequestModels;
using CurrencyConverter.Core.Domain.QueryModels.ResponseModels;
using System.Threading.Tasks;

namespace CurrencyConverter.Core.Domain.Abstractions.QueryHandlers.CurrencyConversionHandlers
{
    public interface IGetConvertedCurrencyQueryHandler
    {
        Task<ValidatableDomainResult<ConvertedCurrency>> HandleAsync(ConvertCurrencyRequest convertCurrencyRequest);
    }
}
