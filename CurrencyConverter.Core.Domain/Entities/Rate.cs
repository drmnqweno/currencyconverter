﻿namespace CurrencyConverter.Core.Domain.Entities
{
    public class Rate
    {
        public string Country { get; set; }
        public decimal Price { get; set; }
    }
}
