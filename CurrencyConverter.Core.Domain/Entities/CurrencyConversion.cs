﻿using System;
using System.Collections.Generic;

namespace CurrencyConverter.Core.Domain.Entities
{
    public class CurrencyConversion
    {
        public string Country { get; set; }
        public DateTime Date { get; set; }
        public DateTimeOffset TimeLastUpdated { get; set; }
        public IEnumerable<Rate> Rates { get; set; }
    }
}
