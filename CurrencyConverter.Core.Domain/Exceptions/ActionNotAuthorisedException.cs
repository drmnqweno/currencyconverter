﻿using System;

namespace CurrencyConverter.Core.Domain.Exceptions
{
    public class ActionNotAuthorisedException : Exception
    {
        public ActionNotAuthorisedException(string message)
            : base(message)
        {

        }

    }
}
