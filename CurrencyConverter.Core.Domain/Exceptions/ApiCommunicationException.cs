﻿using System;

namespace CurrencyConverter.Core.Domain.Exceptions
{
    public class ApiCommunicationException : Exception
    {
        private static string _defaultMessage = "An error occured while trying to access the resource";

        public ApiCommunicationException(Exception exception)
            : this(_defaultMessage, exception)
        { }

        public ApiCommunicationException(string message, Exception exception)
            : base(message, exception)
        { }
    }
}
