﻿using System;

namespace CurrencyConverter.Core.Domain.Exceptions
{
    public class GenericDomainException : Exception
    {
        private static string _defaultMessage = "Could not process this request, please try again later.";

        public GenericDomainException(string message)
            : base(message)
        {

        }

        public GenericDomainException(string message, Exception exception)
            : base(message, exception)
        { }
    }
}
