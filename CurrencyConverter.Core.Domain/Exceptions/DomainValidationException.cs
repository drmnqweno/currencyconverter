﻿using System;
using System.Collections.Generic;

namespace CurrencyConverter.Core.Domain.Exceptions
{
    public class DomainValidationException : Exception
    {
        public IDictionary<string, string[]> ValidationMessages { get; }
        public DomainValidationException(Dictionary<string, string[]> validationMessages)
            : base("Validation errors")
        {
            ValidationMessages = validationMessages;
        }
    }
}
