﻿using CurrencyConverter.Core.Domain.QueryModels.ResponseModels;
using MediatR;

namespace CurrencyConverter.Core.Domain.QueryModels.RequestModels
{
    public class ConvertCurrencyRequest : IRequest<ValidatableDomainResult<ConvertedCurrency>>
    {
        public string BaseCurrency { get; set; }
        public string TargetCurrency { get; set; }
        public decimal Price { get; set; }
    }
}
