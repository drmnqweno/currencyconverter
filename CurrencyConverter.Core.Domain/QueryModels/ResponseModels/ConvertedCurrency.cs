﻿using System;

namespace CurrencyConverter.Core.Domain.QueryModels.ResponseModels
{
    public class ConvertedCurrency
    {
        public string BaseCurrency { get; set; }
        public decimal BaseCurrencyRate { get; set; }
        public string TargetBase { get; set; }
        public decimal TargetRate { get; set; }
        public DateTime DateLastUpdated { get; set; }
        public DateTime TimeLastUpdated { get; set; }
    }
}
