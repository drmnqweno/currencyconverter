﻿using System.Collections.Generic;

namespace CurrencyConverter.Core.Domain.QueryModels
{
    public class ValidatableDomainResult<T>
    {
        public readonly IEnumerable<string> ValidationMessages;
        public readonly T Data;

        public ValidatableDomainResult(T result)
        {
            Data = result;
        }

        public ValidatableDomainResult(IEnumerable<string> validationMesages)
        {
            ValidationMessages = validationMesages;
        }
    }
}
