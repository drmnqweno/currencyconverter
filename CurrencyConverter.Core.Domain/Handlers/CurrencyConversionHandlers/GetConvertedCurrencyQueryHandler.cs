﻿using CurrencyConverter.Core.Domain.Adaptors;
using CurrencyConverter.Core.Domain.Enums;
using CurrencyConverter.Core.Domain.QueryModels;
using CurrencyConverter.Core.Domain.QueryModels.RequestModels;
using CurrencyConverter.Core.Domain.QueryModels.ResponseModels;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CurrencyConverter.Core.Domain.Handlers.CurrencyConversionHandlers
{
    public class GetConvertedCurrencyQueryHandler : IRequestHandler<ConvertCurrencyRequest, ValidatableDomainResult<ConvertedCurrency>> //: IGetConvertedCurrencyQueryHandler
    {
        private readonly ITrainlineCurrencyAdaptor _trainlineCurrencyAdaptor;
        private readonly ILogger<GetConvertedCurrencyQueryHandler> _logger;

        public GetConvertedCurrencyQueryHandler(ITrainlineCurrencyAdaptor trainlineCurrencyAdaptor, ILogger<GetConvertedCurrencyQueryHandler> logger)
        {
            _trainlineCurrencyAdaptor = trainlineCurrencyAdaptor;
            _logger = logger;
        }

        public async Task<ValidatableDomainResult<ConvertedCurrency>> Handle(ConvertCurrencyRequest request,CancellationToken cancellationToken)
        {
            _ = Enum.TryParse<SupportedCurrencies>(request.BaseCurrency.ToUpper(), out var supportedCurrency);

            var currencyRates = await _trainlineCurrencyAdaptor.GetExchangeRatesAsync(supportedCurrency.ToString());

            if (currencyRates == null)
            {
                return default;
            }

            var targetCurrency = currencyRates.Rates?.SingleOrDefault(m => m.Country?.ToUpper() == request.TargetCurrency?.ToUpper());

            if (targetCurrency == null)
            {
                _logger.LogError($"Missing currency. Target currency: {request.TargetCurrency};");
                return new ValidatableDomainResult<ConvertedCurrency>(new List<string> { $"Unsupported currency conversion: Source Currency- {request.BaseCurrency}; Target Currency - {request.TargetCurrency}" });
            }

            var targetRate = targetCurrency.Price * request.Price;

            return new ValidatableDomainResult<ConvertedCurrency>(new ConvertedCurrency
            {
                BaseCurrency = request.BaseCurrency,
                BaseCurrencyRate = request.Price,
                TargetBase = request.TargetCurrency,
                TargetRate = targetRate,
                DateLastUpdated = currencyRates.Date,
                TimeLastUpdated = currencyRates.TimeLastUpdated.DateTime
            });
        }
    }
}
