﻿using CurrencyConverter.Core.Domain.Entities;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace CurrencyConverter.Core.Domain.Handlers.Comparer
{
    public class CurrencyComparer : IEqualityComparer<Rate>
    {
        public bool Equals(Rate x, Rate y)
        {
            return x.Country == y.Country;
        }

        public int GetHashCode([DisallowNull] Rate obj)
        {
            return obj.Country.GetHashCode();
        }
    }
}
