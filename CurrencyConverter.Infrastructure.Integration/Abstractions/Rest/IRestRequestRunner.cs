﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CurrencyConverter.Infrastructure.Integration.Abstractions.Rest
{
    public interface IRestRequestRunner
    {
        Task<RestResponse<TSuccess, TFailure>> ExecuteAsync<TSuccess, TFailure>(RestRequest restRequest);

        void Configure(Action<HttpClient> client);
    }
}
