﻿using System.Net.Http;

namespace CurrencyConverter.Infrastructure.Integration.Abstractions.Rest
{
    public class RestRequest
    {
        public HttpMethod HttpMethod { get; set; }
        public string Resource { get; set; }
        public object Content { get; set; }
    }
}
