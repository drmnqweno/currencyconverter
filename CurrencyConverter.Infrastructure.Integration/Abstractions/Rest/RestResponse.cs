﻿using System.Net;

namespace CurrencyConverter.Infrastructure.Integration.Abstractions.Rest
{
    public class RestResponse<TSuccess, TFailure>
    {
        public readonly TSuccess Success;
        public readonly TFailure Failure;
        private HttpStatusCode _statusCode;

        public RestResponse(TSuccess success)
        {
            Success = success;
            _statusCode = HttpStatusCode.OK;
        }

        public RestResponse(TFailure failure, HttpStatusCode statusCode)
        {
            Failure = failure;
            _statusCode = statusCode;
        }

        public bool IsSuccess()
        {
            return _statusCode == HttpStatusCode.OK;
        }

        public bool IsResourceNotFound()
        {
            return _statusCode == HttpStatusCode.NotFound;
        }

        public bool IsBadRequest()
        {
            return _statusCode == HttpStatusCode.BadRequest;
        }
    }
}
