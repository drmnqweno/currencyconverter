﻿using CurrencyConverter.Core.Domain.Entities;
using CurrencyConverter.Core.Domain.Enums;
using CurrencyConverter.Infrastructure.Integration.Proxy.Trainline;
using System;
using System.Collections.Generic;

namespace CurrencyConverter.Infrastructure.Integration.Mappers
{
    public static class CurrencyResponseMapper
    {
        public static CurrencyConversion MapTo(CurrencyRatesProxy currencyRatesProxy)
        {

            if (currencyRatesProxy == null)
            {
                throw new ArgumentNullException(nameof(currencyRatesProxy));
            }

            return new CurrencyConversion
            {
                Country = currencyRatesProxy.Base,
                Date = currencyRatesProxy.Date,
                TimeLastUpdated = DateTimeOffset.FromUnixTimeSeconds(currencyRatesProxy.TimeLastUpdated),
                Rates = new List<Rate>
                {
                    new Rate
                    {
                        Price =currencyRatesProxy.Rates.Eur,
                        Country= SupportedCurrencies.EUR.ToString(),
                    },
                     new Rate
                    {
                        Price =currencyRatesProxy.Rates.Gbp,
                        Country= SupportedCurrencies.GBP.ToString(),
                    },
                      new Rate
                    {
                        Price =currencyRatesProxy.Rates.Eur,
                        Country= SupportedCurrencies.USD.ToString(),
                    }
                }
            };
        }
    }
}
