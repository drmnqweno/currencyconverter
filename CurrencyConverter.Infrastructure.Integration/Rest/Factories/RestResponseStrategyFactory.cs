﻿using CurrencyConverter.Infrastructure.Integration.Rest.RestResponseStrategies.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace CurrencyConverter.Infrastructure.Integration.Rest.Factories
{
    public class RestResponseStrategyFactory : IRestResponseFactory
    {
        private readonly IEnumerable<IRestHtttpResponseStrategy> _restResponseStrategies;

        public RestResponseStrategyFactory(IEnumerable<IRestHtttpResponseStrategy> restHtttpResponseStrategies)
        {
            _restResponseStrategies = restHtttpResponseStrategies;
        }

        public IRestHtttpResponseStrategy GetRestResponseHandlerStrategy(HttpStatusCode httpStatusCode)
        {
            var httpResponseHandler = _restResponseStrategies.SingleOrDefault(m => m.IsStrategyFor(httpStatusCode));

            if (httpResponseHandler == null)
            {
                throw new ArgumentNullException($"{nameof(httpResponseHandler)} :  Response code-{httpStatusCode}");
            }

            return httpResponseHandler;
        }
    }
}
