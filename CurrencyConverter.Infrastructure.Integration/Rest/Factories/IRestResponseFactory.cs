﻿using CurrencyConverter.Infrastructure.Integration.Rest.RestResponseStrategies.Abstractions;
using System.Net;

namespace CurrencyConverter.Infrastructure.Integration.Rest.Factories
{
    public interface IRestResponseFactory
    {
        IRestHtttpResponseStrategy GetRestResponseHandlerStrategy(HttpStatusCode httpStatusCode);
    }
}
