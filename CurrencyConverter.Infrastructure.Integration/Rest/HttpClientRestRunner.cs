﻿using CurrencyConverter.Core.Domain.Exceptions;
using CurrencyConverter.Infrastructure.Integration.Abstractions.Rest;
using CurrencyConverter.Infrastructure.Integration.Rest.Factories;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CurrencyConverter.Infrastructure.Integration.Rest
{
    public class HttpClientRestRunner : IRestRequestRunner
    {
        private readonly HttpClient _httpClient;
        private readonly IRestResponseFactory _restResponseFactory;
        public readonly ILogger<HttpClientRestRunner> _logger;

        public HttpClientRestRunner(ILogger<HttpClientRestRunner> logger, HttpClient httpClientFactory, IRestResponseFactory restResponseFactory)
        {
            _httpClient = httpClientFactory;
            _logger = logger;
            _restResponseFactory = restResponseFactory;
        }

        public void Configure(Action<HttpClient> clientConfiguration)
        {
            clientConfiguration?.Invoke(_httpClient);
        }

        public async Task<RestResponse<TSuccess, TFailure>> ExecuteAsync<TSuccess, TFailure>(RestRequest restRequest)
        {
            var resourceLocation = _httpClient.BaseAddress + restRequest.Resource;

            try
            {
                var serialisedRequest = JsonSerializer.Serialize(restRequest.Content);
                var encodedRequestContent = new StringContent(serialisedRequest, Encoding.UTF8, "application/json");

                var requestMessage = new HttpRequestMessage
                {
                    Content = encodedRequestContent,
                    RequestUri = new Uri(resourceLocation),
                    Method = restRequest.HttpMethod
                };

                var response = await _httpClient.SendAsync(requestMessage);

                var restResponseStrategy = _restResponseFactory.GetRestResponseHandlerStrategy(response.StatusCode);

                var result = await restResponseStrategy.HandleAsync<TSuccess, TFailure>(response);

                return result;

            }
            catch (HttpRequestException exception)
            {
                _logger.LogError(exception, exception.Message);
                throw new ApiCommunicationException("An error occurred while trying to the request.", exception);
            }
        }
    }
}
