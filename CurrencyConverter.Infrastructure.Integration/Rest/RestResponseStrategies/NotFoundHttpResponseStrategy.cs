﻿using CurrencyConverter.Infrastructure.Integration.Abstractions.Rest;
using CurrencyConverter.Infrastructure.Integration.Rest.RestResponseStrategies.Abstractions;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MAW.Infrastructure.Integration.Rest.Strategies
{
    public class NotFoundHttpResponseStrategy : AbstractHttpResponseStrategy
    {
        private readonly ILogger _logger;

        public NotFoundHttpResponseStrategy(ILogger<NotFoundHttpResponseStrategy> logger)
        {
            _logger = logger;
        }

        public override async Task<RestResponse<TSuccess, TError>> HandleAsync<TSuccess, TError>(HttpResponseMessage httpResponseMessage)
        {
            _logger.LogDebug("Not found handler");

            return new RestResponse<TSuccess, TError>(default, HttpStatusCode.NotFound);
        }

        public override bool IsStrategyFor(HttpStatusCode httpStatusCode)
        {
            return httpStatusCode == HttpStatusCode.NotFound || httpStatusCode == HttpStatusCode.NoContent;
        }
    }
}
