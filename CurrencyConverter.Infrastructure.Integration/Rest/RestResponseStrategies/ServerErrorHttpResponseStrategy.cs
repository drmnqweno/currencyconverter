﻿using CurrencyConverter.Infrastructure.Integration.Abstractions.Rest;
using CurrencyConverter.Infrastructure.Integration.Rest.RestResponseStrategies.Abstractions;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MAW.Infrastructure.Integration.Rest.Strategies
{
    public class ServerErrorHttpResponseStrategy : AbstractHttpResponseStrategy
    {
        private readonly ILogger _logger;

        public ServerErrorHttpResponseStrategy(ILogger<ServerErrorHttpResponseStrategy> logger)
        {
            _logger = logger;
        }

        public override async Task<RestResponse<TSuccess, TError>> HandleAsync<TSuccess, TError>(HttpResponseMessage httpResponseMessage)
        {

            var responseContent = await GetContentAsync(httpResponseMessage);

            _logger.LogDebug("Internal server error: {@data}", responseContent);

            var result = Deserialise<TError>(responseContent);

            return new RestResponse<TSuccess, TError>(result, HttpStatusCode.BadRequest);
        }

        public override bool IsStrategyFor(HttpStatusCode httpStatusCode)
        {
            return httpStatusCode == HttpStatusCode.InternalServerError;
        }
    }
}
