﻿using CurrencyConverter.Core.Domain.Exceptions;
using CurrencyConverter.Infrastructure.Integration.Abstractions.Rest;
using CurrencyConverter.Infrastructure.Integration.Rest.RestResponseStrategies.Abstractions;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace CurrencyConverter.Infrastructure.Integration.Rest.RestResponseStrategies
{
    public class NotAuthorisedHttpResponseStrategy : AbstractHttpResponseStrategy
    {
        public override Task<RestResponse<TSuccess, TError>> HandleAsync<TSuccess, TError>(HttpResponseMessage httpResponseMessage)
        {
            throw new ActionNotAuthorisedException("Not authorised");
        }

        public override bool IsStrategyFor(HttpStatusCode httpStatusCode)
        {
            return httpStatusCode == HttpStatusCode.Unauthorized;
        }
    }
}
