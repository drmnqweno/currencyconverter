﻿using CurrencyConverter.Infrastructure.Integration.Abstractions.Rest;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace CurrencyConverter.Infrastructure.Integration.Rest.RestResponseStrategies.Abstractions
{
    public interface IRestHtttpResponseStrategy
    {
        bool IsStrategyFor(HttpStatusCode httpStatusCode);

        Task<RestResponse<TSuccess, TError>> HandleAsync<TSuccess, TError>(HttpResponseMessage httpResponseMessage);
    }
}
