﻿using CurrencyConverter.Infrastructure.Integration.Abstractions.Rest;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace CurrencyConverter.Infrastructure.Integration.Rest.RestResponseStrategies.Abstractions
{
   public abstract class AbstractHttpResponseStrategy : IRestHtttpResponseStrategy
    {

        public abstract Task<RestResponse<TSuccess, TError>> HandleAsync<TSuccess, TError>(HttpResponseMessage httpResponseMessage);
        public abstract bool IsStrategyFor(HttpStatusCode httpStatusCode);

        protected async Task<string> GetContentAsync(HttpResponseMessage httpResponseMessage)
        {
            var content = await httpResponseMessage.Content.ReadAsStringAsync();

            return content;
        }

        protected T Deserialise<T>(string content)
        {
            var response = JsonSerializer.Deserialize<T>(content);

            return response;
        }
    }
}
