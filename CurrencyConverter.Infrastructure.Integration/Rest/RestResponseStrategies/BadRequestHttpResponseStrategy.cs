﻿using CurrencyConverter.Infrastructure.Integration.Abstractions.Rest;
using CurrencyConverter.Infrastructure.Integration.Rest.RestResponseStrategies.Abstractions;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace CurrencyConverter.Infrastructure.Integration.Rest.RestResponseStrategies
{
    public class BadRequestHttpResponseStrategy : AbstractHttpResponseStrategy
    {
        private readonly ILogger<BadRequestHttpResponseStrategy> _logger;

        public BadRequestHttpResponseStrategy(ILogger<BadRequestHttpResponseStrategy> logger)
        {
            _logger = logger;
        }

        public override async Task<RestResponse<TSuccess, TError>> HandleAsync<TSuccess, TError>(HttpResponseMessage httpResponseMessage)
        {
            var responseContent = await GetContentAsync(httpResponseMessage);
           
            _logger.LogDebug("Bad request: {@badRequestData}", responseContent);

            var responseResult = Deserialise<TError>(responseContent);
            var restResponse = new RestResponse<TSuccess, TError>(responseResult, HttpStatusCode.BadRequest);

            return restResponse;
        }

        public override bool IsStrategyFor(HttpStatusCode httpStatusCode)
        {
            return httpStatusCode == HttpStatusCode.BadRequest;
        }
    }
}
