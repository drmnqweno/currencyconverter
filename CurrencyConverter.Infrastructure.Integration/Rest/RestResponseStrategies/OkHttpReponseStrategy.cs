﻿using CurrencyConverter.Infrastructure.Integration.Abstractions.Rest;
using CurrencyConverter.Infrastructure.Integration.Rest.RestResponseStrategies.Abstractions;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MAW.Infrastructure.Integration.Rest.Strategies
{
    public class OkHttpReponseStrategy : AbstractHttpResponseStrategy
    {
        public override async Task<RestResponse<TSuccess, TError>> HandleAsync<TSuccess, TError>(HttpResponseMessage httpResponseMessage)
        {
            var responseContent = await GetContentAsync(httpResponseMessage);

            if (string.IsNullOrEmpty(responseContent))
            {
                return default;
            }
            
            var result = Deserialise<TSuccess>(responseContent);

            var restResponse = new RestResponse<TSuccess, TError>(result);

            return restResponse;
        }

        public override bool IsStrategyFor(HttpStatusCode httpStatusCode)
        {
            var response = httpStatusCode == HttpStatusCode.OK;

            return response;
        }
    }
}

