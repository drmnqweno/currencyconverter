﻿using CurrencyConverter.Core.Domain.Adaptors;
using CurrencyConverter.Core.Domain.Entities;
using CurrencyConverter.Infrastructure.Integration.Abstractions.Rest;
using CurrencyConverter.Infrastructure.Integration.Config;
using CurrencyConverter.Infrastructure.Integration.Mappers;
using CurrencyConverter.Infrastructure.Integration.Proxy.Trainline;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CurrencyConverter.Infrastructure.Integration.Adaptors
{
    public class TrainlineCurrencyAdaptorTwo : ITrainlineCurrencyAdaptorTwo
    {
        private readonly ILogger<TrainlineCurrencyAdaptor> _logger;
        private readonly IRestRequestRunner _restRequestRunner;

        public TrainlineCurrencyAdaptorTwo(ILogger<TrainlineCurrencyAdaptor> logger,
            IOptions<TrainlineCurrencyConverterConfig> endpointConfigOptions
            , IRestRequestRunner restRequestRunner)
        {
            _logger = logger;
            _restRequestRunner = restRequestRunner;

            _restRequestRunner.Configure(m => m.BaseAddress = new Uri(endpointConfigOptions.Value.Host));
        }

        public async Task<CurrencyConversion> GetExchangeRatesAsync(string currency)
        {
          await  Task.Delay(1000);

            _logger.LogDebug("Calling the rest runner: GetExchangeRatesAsync");

            var restRequest = new RestRequest
            {
                Resource = $"exchangerates/api/latest/{currency}.json",
                HttpMethod = HttpMethod.Get
            };

            var restResponse = await _restRequestRunner.ExecuteAsync<CurrencyRatesProxy, string>(restRequest);

            _logger.LogDebug("Call succeful: GetExchangeRatesAsync");

            if (restResponse == null)
            {
                throw new ArgumentNullException(nameof(restResponse));
            }

            if (restResponse.IsResourceNotFound())
            {
                return null;
            }

            return CurrencyResponseMapper.MapTo(restResponse.Success);
        }
    }
}
