﻿namespace CurrencyConverter.Infrastructure.Integration.Config
{
    public abstract class AbstractIntegrationEndpoint
    {
        public string Client { get; set; }
        public string Host { get; set; }
    }
}
