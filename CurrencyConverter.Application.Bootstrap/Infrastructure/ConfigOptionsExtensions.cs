﻿using Microsoft.Extensions.DependencyInjection;
using CurrencyConverter.Infrastructure.Integration.Config;
using Microsoft.Extensions.Configuration;

namespace CurrencyConverter.Application.Bootstrap.Infrastructure
{
    public static class ConfigOptionsExtensions
    {
        public static void AddAdaptorConfigs(this IServiceCollection services,IConfiguration configuration)
        {
            services.Configure<TrainlineCurrencyConverterConfig>(configuration.GetSection(TrainlineCurrencyConverterConfig.Location));
        }
    }
}
