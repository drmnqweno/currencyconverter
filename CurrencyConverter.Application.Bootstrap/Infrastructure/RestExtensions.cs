﻿using CurrencyConverter.Infrastructure.Integration.Abstractions.Rest;
using CurrencyConverter.Infrastructure.Integration.Rest;
using CurrencyConverter.Infrastructure.Integration.Rest.Factories;
using CurrencyConverter.Infrastructure.Integration.Rest.RestResponseStrategies;
using CurrencyConverter.Infrastructure.Integration.Rest.RestResponseStrategies.Abstractions;
using MAW.Infrastructure.Integration.Rest.Strategies;
using Microsoft.Extensions.DependencyInjection;

namespace CurrencyConverter.Application.Bootstrap.Infrastructure
{
    public static class RestExtensions
    {
        public static void AddRestRunner(this IServiceCollection services)
        {
            services.AddTransient<IRestResponseFactory, RestResponseStrategyFactory>();

            services.AddTransient<IRestHtttpResponseStrategy, NotAuthorisedHttpResponseStrategy>();
            services.AddTransient<IRestHtttpResponseStrategy, NotFoundHttpResponseStrategy>();
            services.AddTransient<IRestHtttpResponseStrategy, OkHttpReponseStrategy>();
            services.AddTransient<IRestHtttpResponseStrategy, ServerErrorHttpResponseStrategy>();
            services.AddTransient<IRestHtttpResponseStrategy, BadRequestHttpResponseStrategy>();

            services.AddHttpClient<IRestRequestRunner, HttpClientRestRunner>()
                .AddPolicyHandler(IntegrationPolicies.GetRetryJittertPolicy());
        }
    }
}
