﻿using CurrencyConverter.Core.Domain.Adaptors;
using CurrencyConverter.Infrastructure.Integration.Adaptors;
using Microsoft.Extensions.DependencyInjection;

namespace CurrencyConverter.Application.Bootstrap.Domain
{
    public static class AdaptorExtensions
    {
        public static void AddServiceAdaptor(this IServiceCollection services)
        {
            services.AddTransient<ITrainlineCurrencyAdaptor, TrainlineCurrencyAdaptor>();
            services.AddTransient<ITrainlineCurrencyAdaptorTwo, TrainlineCurrencyAdaptorTwo>();
        }
    }
}
