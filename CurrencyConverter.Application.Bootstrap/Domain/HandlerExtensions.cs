﻿using CurrencyConverter.Core.Domain.Abstractions.QueryHandlers.CurrencyConversionHandlers;
using CurrencyConverter.Core.Domain.Handlers.CurrencyConversionHandlers;
using CurrencyConverter.Core.Domain.Validators;
using CurrencyConverter.Core.Domain.Validators.Behaviours;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace CurrencyConverter.Application.Bootstrap.Domain
{
    public static class HandlerExtensions
    {
        public static void AddDomainHandlers(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly(), typeof(GetConvertedCurrencyQueryHandler).Assembly);
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(PipelineBehaviour<,>));
            services.AddValidatorsFromAssembly(typeof(ConvertCurrencyValidator).Assembly);
        }
    }
}
