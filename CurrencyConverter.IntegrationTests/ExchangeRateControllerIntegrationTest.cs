﻿using Api.CurrencyConverter.Controllers;
using CurrencyConverter.Core.Domain.Abstractions.QueryHandlers.CurrencyConversionHandlers;
using CurrencyConverter.Core.Domain.Adaptors;
using CurrencyConverter.Core.Domain.Handlers.CurrencyConversionHandlers;
using CurrencyConverter.IntegrationTests.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;
using Xunit;

namespace CurrencyConverter.IntegrationTests
{
    public class ExchangeRateControllerIntegrationTest : IntegrationWebApplicationFactory, IDisposable
    {

        private WireMockServer _trainLineMockServer;

        public ExchangeRateControllerIntegrationTest()
        {
            _trainLineMockServer = WireMockServer.Start(9090);
        }

        [Fact]
        public async Task Get_ValidCurrency_ReturnsOk()
        {
            //Arrange
            var request = Request.Create()
                .UsingGet()
                .WithPath("/exchangerates/api/latest/USD.json");

            var resultData = await File.ReadAllBytesAsync("Resources/GetExchangeRatesReturnsValidData.json");

            _trainLineMockServer.Given(request)
                .RespondWith(Response.Create()
                .WithHeader("content-type", "application/json")
           .WithBody(resultData));

            var mediatr = Services.GetRequiredService<IMediator>();
            var logger = Services.GetRequiredService<ILogger<ExchangeRatesController>>();

            var ratesController = new ExchangeRatesController(logger, mediatr);

            //Act

            var response = await ratesController.Get(1, "USD", "EUR");
            var okResult = response as OkObjectResult;
            //Assert
            Assert.NotNull(response);
            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode.Value);

        }

        [Fact]
        public async Task Get_InvalidCurrencyCode_ReturnsNotFound()
        {
            //Arrange
            var request = Request.Create()
                .UsingGet()
                .WithPath("/exchangerates/api/latest/EUR.json");

            _trainLineMockServer.Given(request)
                .RespondWith(Response.Create()
                .WithHeader("content-type", "application/json")
                .WithStatusCode(System.Net.HttpStatusCode.NotFound)
                .WithBody(string.Empty));

            var mediatr = Services.GetRequiredService<IMediator>();
            var logger = Services.GetRequiredService<ILogger<ExchangeRatesController>>();

            var ratesController = new ExchangeRatesController(logger, mediatr);

            //Act

            var response = await ratesController.Get(1, "EUR", "USD");
            var notFound = response as NotFoundResult;

            //Assert
            Assert.NotNull(response);
            Assert.NotNull(notFound);
            Assert.Equal(404, notFound.StatusCode);
        }

        protected override void Dispose(bool disposing)
        {
            _trainLineMockServer.Stop();
            _trainLineMockServer.Dispose();

            base.Dispose(disposing);
        }
    }
}
