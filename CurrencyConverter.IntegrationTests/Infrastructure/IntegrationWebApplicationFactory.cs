﻿using Api.CurrencyConverter;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System.IO;

namespace CurrencyConverter.IntegrationTests.Infrastructure
{
    public class IntegrationWebApplicationFactory : WebApplicationFactory<Startup>
    {
        private int _port;
        public IntegrationWebApplicationFactory()
        {
            
        }

        protected override IHostBuilder CreateHostBuilder()
        {
            return Host
                  .CreateDefaultBuilder()
                  .ConfigureAppConfiguration(config =>
                  {
                      config.SetBasePath(Directory.GetCurrentDirectory())
                      .AddJsonFile("appsettings.test.json", false, false);
                  })
                  .UseContentRoot(Directory.GetCurrentDirectory())
                  .ConfigureWebHostDefaults(opt =>
                  {
                      opt.UseStartup<Startup>();
                      opt.UseKestrel();
                  });
            
        }
    }
}
