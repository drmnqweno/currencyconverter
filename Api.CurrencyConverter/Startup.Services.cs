using CurrencyConverter.Application.Bootstrap.Domain;
using CurrencyConverter.Application.Bootstrap.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System.Reflection;

namespace Api.CurrencyConverter
{
    public partial class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // services.AddMediatR(typeof(Startup));
            //services.AddMediatR(typeof(AddEducationCommand).GetTypeInfo().Assembly);
            

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Api.CurrencyConverter", Version = "v1" });
            });

            services.AddApiVersioning(v =>
            {
                v.ReportApiVersions = true;
                v.AssumeDefaultVersionWhenUnspecified = true;
                v.DefaultApiVersion = ApiVersion.Default;
            });

            services.AddRestRunner();
            services.AddDomainHandlers();
            services.AddServiceAdaptor();
            services.AddAdaptorConfigs(Configuration);
        }
    }
}
