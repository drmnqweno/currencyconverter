using Microsoft.Extensions.Configuration;

namespace Api.CurrencyConverter
{
    public partial class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
    }
}
