﻿using CurrencyConverter.Core.Domain.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Api.CurrencyConverter.Middleware
{
    internal class GlobalExceptionHandler
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<GlobalExceptionHandler> _logger;
        private ProblemDetails _genericProblemDetails;

        public GlobalExceptionHandler(RequestDelegate next, ILogger<GlobalExceptionHandler> logger)
        {
            _next = next;
            _logger = logger;

            _genericProblemDetails = new ProblemDetails
            {
                Detail = "Could not process your request, please try again later",
                Status = 500,
                Title = "Internal server error"
            };
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (DomainValidationException domainException)
            {
                _logger.LogError(domainException.Message);

                var validationProblemDetails = new ValidationProblemDetails(domainException.ValidationMessages)
                {
                    Title = domainException.Message
                };

                httpContext.Response.ContentType = "application/json";
                httpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;

                await httpContext.Response.WriteAsync(JsonConvert.SerializeObject(validationProblemDetails));

            }
            catch (GenericDomainException exception)
            {
                _logger.LogError(exception, exception.Message);

                await HandleExceptionAsync(httpContext, _genericProblemDetails);
            }
            catch (ApiCommunicationException exception)
            {
                _logger.LogError(exception, exception.Message);

                await HandleExceptionAsync(httpContext, new ProblemDetails
                {
                    Title = "Internal server error",
                    Detail = exception.Message
                });
            }
            catch (ActionNotAuthorisedException ex)
            {
                httpContext.Response.ContentType = "application/json";
                httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await httpContext.Response.WriteAsync(JsonConvert.SerializeObject(new { Code = 401, Error = ex.Message }));
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, exception.Message);
                await HandleExceptionAsync(httpContext, _genericProblemDetails);
            }
        }
        private async Task HandleExceptionAsync(HttpContext httpContext, ProblemDetails problemDetails)
        {
            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            await httpContext.Response.WriteAsync(JsonConvert.SerializeObject(problemDetails));
        }
    }
}
