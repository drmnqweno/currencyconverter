﻿using System;

namespace Api.CurrencyConverter.Models
{
    public class ExchangeRateResultModel
    {
        public string BaseCurrency { get; set; }
        public decimal BaseRate { get; set; }
        public string TargetCurrency { get; set; }
        public decimal TargetRate { get; set; }
        public DateTime DateLastUpdated { get; set; }
        public DateTime TimeLastUpdated { get; set; }
    }
}
