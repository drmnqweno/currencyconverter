﻿namespace Api.CurrencyConverter.Models
{
    public class RateResultModel
    {
        public string CountryCode { get; set; }
        public decimal Rate { get; set; }
    }
}
