﻿using Api.CurrencyConverter.Models;
using CurrencyConverter.Core.Domain.QueryModels.RequestModels;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.CurrencyConverter.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/ExchangeRates")]
    [ApiController]
    public class ExchangeRatesController : ControllerBase
    {
        private readonly ILogger<ExchangeRatesController> _logger;
        private readonly IMediator _mediatr;
        public ExchangeRatesController(ILogger<ExchangeRatesController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediatr = mediator;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ExchangeRateResultModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ProblemDetails))]
        public async Task<IActionResult> Get(decimal price, string basecurrency, string targetcurrency)
        {
            var result = await _mediatr.Send(new ConvertCurrencyRequest
            {
                Price = price,
                BaseCurrency = basecurrency,
                TargetCurrency = targetcurrency
            });

            if (result == null)
            {
                return NotFound();
            }

            if (result.ValidationMessages != null && result.ValidationMessages.Any())
            {
                var validationMessages = new Dictionary<string, string[]>
                {
                    { "ConversionErrors", result.ValidationMessages.ToArray() }
                };

                var validationResult = new ValidationProblemDetails(validationMessages)
                {
                    Title = "Invalid currency request",
                    Detail = "The request failed due to validation messages below"
                };

                return BadRequest(validationResult);
            }

            return Ok(new ExchangeRateResultModel
            {
                BaseCurrency = result.Data.BaseCurrency,
                BaseRate = result.Data.BaseCurrencyRate,
                DateLastUpdated = result.Data.DateLastUpdated,
                TargetCurrency = result.Data.TargetBase,
                TargetRate = result.Data.TargetRate,
                TimeLastUpdated = result.Data.TimeLastUpdated
            });
        }
    }
}
