# README #

### What is this repository for? ###

This repository contains an MVP API to convert currency, an example API request is below.
api/v1/ExchangeRates?price=1&basecurrency=eur&targetcurrency=usd

### Set up ###

* Check out the source code
* Make sure you have .Net 5 installed on your machine
* Trainline currency API is a dependency
* Open and run the solution on a code editor of your choice

### Docker image ###

* Install the docker engine or docker desktop on your machine: https://docs.docker.com/engine/install/
* Run this docker commands: docker run -p 5590:5000 biyana14/africa:v1
* Use Postman to test the API:[GET] http://localhost:5590/api/v1/ExchangeRates?price=1&basecurrency=eur&targetcurrency=usd

### Testing tools ###

* Postman
* Fiddler to test resiliency
* Unit and integration tests

### Future improvements###

* Add more unit and integration tests
* Add logging (Serilog) 
* Add fluent validation to run domain validation rules (done)
* Add Polly's memory caching policy to improve resiliance and availability
* Integration exception handling can be better
* Add MediatR for domain handlers(CQRS) (done)