﻿using System.Text.Json.Serialization;

namespace CurrencyConverter.Infrastructure.Integration.Proxy.Trainline
{
    public class RatesProxy
    {
        [JsonPropertyName("GBP")]
        public decimal Gbp { get; set; }

        [JsonPropertyName("EUR")]
        public decimal Eur { get; set; }

        [JsonPropertyName("USD")]
        public decimal Usd { get; set; }
    }
}
