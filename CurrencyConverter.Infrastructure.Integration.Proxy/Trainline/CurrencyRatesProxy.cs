﻿using System;
using System.Text.Json.Serialization;

namespace CurrencyConverter.Infrastructure.Integration.Proxy.Trainline
{
    public class CurrencyRatesProxy
    {
        [JsonPropertyName("base")]
        public string Base { get; set; }
        [JsonPropertyName("date")]
        public DateTime Date { get; set; }
        [JsonPropertyName("time_last_updated")]
        public long TimeLastUpdated { get; set; }
        [JsonPropertyName("rates")]
        public RatesProxy Rates { get; set; }
    }
}
