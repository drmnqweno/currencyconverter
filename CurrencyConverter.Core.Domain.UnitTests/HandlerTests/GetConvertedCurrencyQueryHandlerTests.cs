﻿using CurrencyConverter.Core.Domain.Adaptors;
using CurrencyConverter.Core.Domain.Entities;
using CurrencyConverter.Core.Domain.Handlers.CurrencyConversionHandlers;
using CurrencyConverter.Core.Domain.QueryModels.RequestModels;
using FakeItEasy;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace CurrencyConverter.Core.Domain.UnitTests.HandlerTests
{

    public class GetConvertedCurrencyQueryHandlerTests
    {
        [Fact]
        public async Task HandleAsync_UsdCurrency_ReturnsSuccess()
        {
            //Arrange
            var fakeTrainLineAdaptor = A.Fake<ITrainlineCurrencyAdaptor>();
            var fakeLogger = A.Fake<ILogger<GetConvertedCurrencyQueryHandler>>();

            var fakeCurrencyConversionResponse = A.Fake<CurrencyConversion>();
            fakeCurrencyConversionResponse.Rates = new List<Rate>
            {
                new Rate{Country = "USD",Price = 5},
                new Rate{Country = "EUR",Price = 4},
                new Rate{Country = "USD",Price = 2}
            };

            A.CallTo(() => fakeTrainLineAdaptor.GetExchangeRatesAsync(A<string>._)).Returns(Task.FromResult(fakeCurrencyConversionResponse));

            var handler = new GetConvertedCurrencyQueryHandler(fakeTrainLineAdaptor, fakeLogger);

            //Act
            var respose = await handler.Handle(new ConvertCurrencyRequest { Price = 2, BaseCurrency = "USD", TargetCurrency = "EUR" }, new System.Threading.CancellationToken());

            //Assert
            Assert.NotNull(respose.Data);
            Assert.Equal(8, respose.Data.TargetRate);
            Assert.Null(respose.ValidationMessages);
        }

        [Fact]
        public async Task HandleAsync_ZARInvalidCurrency_ReturnsNull()
        {
            //Arrange
            var fakeTrainLineAdaptor = A.Fake<ITrainlineCurrencyAdaptor>();
            var fakeLogger = A.Fake<ILogger<GetConvertedCurrencyQueryHandler>>();
            var fakeCurrencyConversionResponse = A.Fake<CurrencyConversion>();

            A.CallTo(() => fakeTrainLineAdaptor.GetExchangeRatesAsync(A<string>._)).Returns(Task.FromResult(fakeCurrencyConversionResponse));

            var handler = new GetConvertedCurrencyQueryHandler(fakeTrainLineAdaptor, fakeLogger);

            //Act
            var respose = await handler.Handle(new ConvertCurrencyRequest { Price = 2, BaseCurrency = "ZAR", TargetCurrency = "EUR" }, new System.Threading.CancellationToken());

            //Assert
            Assert.Null(respose.Data);
            Assert.NotNull(respose.ValidationMessages);
            Assert.NotEmpty(respose.ValidationMessages);
        }
    }
}
